import com.company.Autenticador;
public class Cliente2 implements Autenticador {
    private String nome;
    private boolean status;
    private String senha;
    private int Compras;
    public String getNome() {
        return nome;
    }
    public boolean isStatus() {
        return status;
    }
    public String getSenha() {
        return senha;
    }
    public int getCompras(){
        return this.Compras;
       }
    @Override
    public String toString() {
        return "Cliente2{" +
                "nome='" + nome + '\'' +
                ", status=" + status +
                ", senha='" + senha + '\'' +
                '}';
        }
    @Override
    public boolean autentica(String senha) {
        if(this.senha != senha) {
            System.out.println("Não autenticado");
            return false;
        }
        else{
            System.out.println("Autenticado");
            return true;
        }
        }
    public Cliente2(String nome, boolean status, String senha,int Compras) {
        this.nome = nome;
        this.status = status;
        this.senha = senha;
        this.Compras = Compras;
       }
       }
