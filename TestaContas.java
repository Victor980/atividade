import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class TestaContas {
    public static void main(String [] args) {
        Cliente2 cli1 = new Cliente2("Victor", true, "ddd", 15);
        Cliente2 cli2 = new Cliente2("Mateus", true, "xxx", 10);
        Cliente2 cli3 = new Cliente2("Santos", false, "zzz", 25);
        Cliente2 cli4 = new Cliente2("Oliveira", true, "aaa", 35);
        Cliente2 cli5 = new Cliente2("Maria", true, "ccc", 20);
        Cliente2 cli6 = new Cliente2("Victoria", false, "ggg", 45);
        Cliente2 cli7 = new Cliente2("Marcos", true, "ttt", 55);
        Cliente2 cli8 = new Cliente2("Vinicius", true, "kkk", 60);
        Cliente2 cli9 = new Cliente2("Emilene", false, "rrr", 65);
        Cliente2 cli10 = new Cliente2("Silva", true, "vvv", 75);
        List<Cliente2> clientes = Arrays.asList(cli1, cli2, cli3, cli4, cli5, cli6, cli7, cli8, cli9, cli10);
        List<Cliente2> clientesFiltrados = clientes.stream().filter(cliente -> cliente.getCompras() >= 0).toList();
        for (Cliente2 cliente2 : clientesFiltrados) {
        }
        Comparator<Cliente2> Menor = Comparator.comparing(Cliente2::getCompras);
        List<Cliente2> menosCompras = clientesFiltrados.stream().min(Menor).stream().toList();
        System.out.println("Qual Cliente fez Menos Compras?");
        System.out.println(menosCompras);
        System.out.println("----------------------------------------------------------");
        Comparator<Cliente2> Maior = Comparator.comparing(Cliente2::getCompras);
        List<Cliente2> maisCompras = clientesFiltrados.stream().max(Maior).stream().toList();
        System.out.println("Qual Cliente fez Mais Compras?");
        System.out.println(maisCompras);
        System.out.println("----------------------------------------------------------");
        System.out.println("A media de compras é de : " + clientes.stream().mapToDouble(Cliente2 :: getCompras).average().getAsDouble());
    }
}